//gets user location
function getLocation() {
	function saveLocation(position) {
		userLocation.latit = position.coords.latitude;
		userLocation.longit = position.coords.longitude;
		console.log("Location Updated");
	}
	
	if ('geolocation' in navigator){
		navigator.geolocation.getCurrentPosition(saveLocation);
		refreshContent();
	} else {
		alert("Geolocation is not supported by this browser.");
		refreshContent();
	}
}

// check for html5 storage support
function supports_html5_storage() {
  try {
    return 'localStorage' in window && window['localStorage'] !== null;
  } catch (e) {
    return false;
  }
}

// store current list of places locally in html5 storage
function cachePlaces() {
	console.log("Caching Places");
	refreshContent();
	if (!supports_html5_storage()) {
		alert('Sorry, this browser does not support cached dining data'); 
		return false;
	}
	localStorage["dukeplaces.places"] = JSON.stringify(localPlaces);
	localStorage["dukeplaces.dataVersionNumber"] = dataVersionNumber;
	return true;
}

// pull cached list of places from html5 storage
function fetchPlaces() {
	if (!supports_html5_storage()) { return false; }
	var cachedPlaces = localStorage["dukeplaces.places"];
	var cachedDataVersionNumber = localStorage["dukeplaces.dataVersionNumber"];
	if (( cachedPlaces == null ) || ( cachedDataVersionNumber != dataVersionNumber )) { 
		return false; 
	} else {
		console.log("Fetching Places");
		localPlaces = JSON.parse( cachedPlaces );
		refreshContent();
		return true;
	}
}


// take note that one update has started
function startUpdate() {
	pendingUpdate += 1;
	console.log("Started update: " + pendingUpdate + " still running");
}
// take note that one update had finished, and if all updates are finished, then cache places
function endUpdate() {
	pendingUpdate -=1;
	console.log("Completed update: " + pendingUpdate + " still running");
	if (pendingUpdate === 0) {cachePlaces();}
}

// Get list of places and current open status
function updatePlaces() {
	startUpdate();
	$.getJSON(placesMainAPI, {
		access_token: "59d480ac000b7559a41726eab9318003",
		tag: "dining"
	})
	.done(function (json) {
		var found = false;
		var tempPlace;
		$.each(json, function (i, freshPlace) { // for each of the fresh new places
			tempPlace = { // create a local data entry for the place data
				"local_id": 0,
				"place_id": freshPlace.place_id,
				"name": freshPlace.name,
				"position": {
					"latitude": freshPlace.position.latitude,
					"longitude": freshPlace.position.longitude
				},
				"location": freshPlace.location,
				"open_now": (freshPlace.open == "true" ? true : false),
				"schedule": [],
				"source": "streamer"
			};
			$.each(localPlaces, function (j, cachedPlace) { // search through cached places for a matching place
				if (!found && tempPlace.place_id === cachedPlace.place_id) {
					found = true;
					tempPlace.local_id = j;
					tempPlace.schedule = cachedPlace.schedule;
					tempPlace.tags = cachedPlace.tags;
					localPlaces[j] = tempPlace;
				}
			});
			if (!found) { // if no match is found add place to cached places
				tempPlace.local_id = localPlaces.length;
				localPlaces.push(tempPlace);
			}
			found = false;
		});
	//(funny business happens if above loops don't complete before place detail loops begin)
	setTimeout(updatePlaceDetails,1); // necessary delay to prevent unknown bug in jQuery 
	refreshContent();
	endUpdate();
	});
}

//go through list of places and get details
function updatePlaceDetails(tempPlaceId) { 
	function doRequest(i, item) {
		if (item.source === "streamer") {
			startUpdate();
			$.getJSON(placesDetailAPI, {
				access_token: "59d480ac000b7559a41726eab9318003",
				place_id: item.place_id
			})
			.done(function (json) { // store fresh data in local place database
				var tempSlot;
				var tempDate;
				var tempTime;
				var tempSchedule = [];
				$.each(json.schedule.open_close, function (j, stateChange) {
					if (stateChange.label === "close") {
						if (j == 0) {
							tempDate = stateChange.date;
							tempTime = "12:00 AM";
							console.log("issue with " + item.name);
						} else {
							tempDate = json.schedule.open_close[j-1].date;
							tempTime = json.schedule.open_close[j-1].time;
						}
						tempSlot = [{
							"label": "open",
							"date": tempDate,
							"time": tempTime
						},
						{
							"label": "close",
							"date": stateChange.date,
							"time": stateChange.time
						}];
						tempSchedule.push(tempSlot);
					}
				});
				localPlaces[i]["extensions"] = json.extensions;
				localPlaces[i]["schedule"] = tempSchedule;
				localPlaces[i]["tags"] = json.tags;
				endUpdate();
			});
		}
	}
	
	if (arguments.length == 1) { // If a specific placeId is given, update it
	//	This exists if number of API requests is too high, can be later used to only update 'stale' places
		var found = false;
		$.each(localPlaces, function (j, currentPlace) {
			if (!found && currentPlace.source === "streamer" && currentPlace.place_id === tempPlaceId) {
				found = true;
				doRequest(j, currentPlace);
			}
		});
		if (!found) {
			return false;
		}
	} else { // otherwise update all the place details
		$.each(localPlaces, function (i, item) { // lookup each localPlace
			doRequest(i, item);
		});
	}
	return true;
}

function updateCalendarFeed(calendarFeed) {
	startUpdate();
	var today = new Date();
	today.setHours(0);
	today.setMinutes(0);
	today.setSeconds(0);
	var oneWeek = today;
	oneWeek.setDate(today.getDate() + 7);
    $.getJSON(calendarFeed, {
        'alt': "json",
        'orderby': "starttime",
        'max-results': "30",
        'singleevents': "false",
        'sortorder': "ascending",
        'futureevents': "true",
		'recurrence-expansion-start': today.toISOString(),
		'recurrence-expansion-end': oneWeek.toISOString(),
		'start-min': today.toISOString(),
		'start-max': oneWeek.toISOString()
    })
	.done(function (json) {
		for (k = 0; k<localPlaces.length; k++) {
			if (localPlaces[k].source == "google") { // if the internet request completes, delete google sourced place data from the repository
				localPlaces.splice(k,1);
				k--;
			}
		}
		var tempPlace;
		var tempSchedule;
		var tempStart;
		var tempEnd;
		var tempPosition;
		var tempTags;
		var found = false;
		$.each(json.feed.entry, function (i, freshPlace) {
			tempSchedule = [];
			$.each(freshPlace.gd$when, function (j, timeSlot) {
				tempTags = [];
				tempStart = new Date(timeSlot.startTime);
				tempEnd = new Date(timeSlot.endTime);
				tempSchedule.push([{
					"label": "open",
					"date": (tempStart.getMonth()+1) + "/" + tempStart.getDate() + "/" + tempStart.getFullYear(),
					"time": (((tempStart.getHours()+11)%12)+1) + ":" + ("0" + tempStart.getMinutes()).slice(-2) + " " + (tempStart.getHours() > 11 ? "PM" : "AM")
				},
				{
					"label": "close",
					"date": (tempEnd.getMonth()+1) + "/" + tempEnd.getDate() + "/" + tempEnd.getFullYear(),
					"time": (((tempEnd.getHours()+11)%12)+1) + ":" + ("0" + tempEnd.getMinutes()).slice(-2) + " " + (tempEnd.getHours() > 11 ? "PM" : "AM")
				}]);
			});
			tempPosition = {
				"latitude": 0,
				"longitude": 0
			};
			
			// Start Hard Coded Parameters
			if (freshPlace.gd$where[0].valueString.indexOf("hapel") != -1) {
				tempPosition = {
					"latitude": "36.000941",
					"longitude": "-78.938582"
				};
			}
			if (freshPlace.gd$where[0].valueString.indexOf("annamaker") != -1) {
				tempPosition = {
					"latitude": "35.998784",
					"longitude": "-78.93937"
				};
			}
			if (freshPlace.gd$where[0].valueString.indexOf("eohan") != -1) {
				tempPosition = {
					"latitude": "35.99924",
					"longitude": "-78.938201"
				};
			}
			if (freshPlace.gd$where[0].valueString.indexOf("Telcom") != -1 || freshPlace.gd$where[0].valueString.indexOf("telcom") != -1) {
				tempPosition = {
					"latitude": "36.002773",
					"longitude": "-78.939902"
				};
			}
			if (freshPlace.gd$where[0].valueString.indexOf("West") != -1 || freshPlace.gd$where[0].valueString.indexOf("west") != -1) {
				tempTags.push("west_campus");
			}
			if (freshPlace.gd$who[0].valueString.indexOf("truck") != -1 || freshPlace.gd$who[0].valueString.indexOf("Truck") != -1) {
				tempTags.push("food_truck");
			}
			
			// End Hard Coded Parameters
			
			tempPlace = {
				"local_id": 0,
				"place_id": freshPlace.gCal$uid.value.split("@")[0],
				"name": freshPlace.title.$t,
				"position": tempPosition,
				"location": freshPlace.gd$where[0].valueString,
				"open_now": false,
				"source": "google",
				"extensions": [
					{
						"label": "Details",
						"value": freshPlace.content.$t
					}
				],
				"tags": tempTags,
				"schedule": tempSchedule
			};
			if (freshPlace.gd$eventStatus.value.indexOf("canceled") != -1) found = true; // Skip canceled events, so they don't make it into the cache/display
			$.each(localPlaces, function (k, cachedPlace) {
				if (!found && cachedPlace.source == "google" && tempPlace.place_id === cachedPlace.place_id) { // technically shouldn't occur, since all google events should be removed if internet connection available
					// this case still occurs in some browsers, so this workaround was implemented
					found = true;
					tempPlace.local_id = k;
					localPlaces[k] = tempPlace;
				}
			});
			if (!found) {
				tempPlace.local_id = localPlaces.length;
				localPlaces.push(tempPlace);
			}
			found = false;
		});
		refreshContent();
		endUpdate();
	});
}

var dataVersionNumber = 5; // iterate this number if changes are made to the structure of the browser cache (to force browser cache update and prevent fatal errors)
var placesMainAPI = "https://streamer.oit.duke.edu/places/items.js?callback=?";
var placesDetailAPI = "https://streamer.oit.duke.edu/places/items/show.js?callback=?";
var foodTruckFeed = "https://www.google.com/calendar/feeds/vgkckpl2e04dgvbtn94u1jeuk0%40group.calendar.google.com/public/full?";
var localPlaces = []; // database of all places stored in the clients browser
var userLocation = {};
var pendingUpdate = 0;

fetchPlaces(); // pull places data from browser cache
getLocation(); // get location data (if successful, will refresh page content)
updatePlaces(); // update places via Streamer (on completion calls updatePlaceDetails for each place) then refreshes page content
updateCalendarFeed(foodTruckFeed); // update food truck schedule, and refresh page content
// when all the above functions are complete (0 pending updates) the schedule data is written to the browser cache
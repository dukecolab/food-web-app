function updateFiltersFromGui() { // updates variables based on GUI settings
	campus = $("#campus").val();
	time = $("#time").val();
	daysFromNow = $("#date").val();
}

function filter(item) {
	updateFiltersFromGui();

	//open/closed filter
	isOpen = calcIsOpen(item.place_id, time, daysFromNow);
	if (!isOpen && !showClosed) {return false;}

	//campus filter
	if (campus != "") {
		if (item.tags.indexOf(campus) == -1) {return false;}
	}

	//tag filter
	if (filterTags.length > 0) {
		for (var i = 0; i < filterTags.length; i++) {
			if (item.tags.indexOf(filterTags[i]) == -1) {return false}
		}
	}

	//guess it passed through all the filters!
	return true;
}

function initializeMap(place_title,locationId, latitude, longitude) {
	var myLatlng = new google.maps.LatLng(latitude,longitude);
	var mapOptions = {
		zoom: 17,
		center: myLatlng,
		disableDefaultUI: true,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	}
	var map = new google.maps.Map(document.getElementById(locationId), mapOptions);

	var marker = new google.maps.Marker({
		position: myLatlng,
		map: map,
		title: place_title
	});
	
	var infowindow = new google.maps.InfoWindow({
		content: place_title
	});
	google.maps.event.addListener(marker, 'click', function() {
		infowindow.open(map,marker)
	});
}

// list all open places with distance alongside it (in miles)
function refreshContent() {
	console.log("Refreshing content");
	var sortedPlaces = localPlaces;
	sortedPlaces.sort(compareDist);

	//Updates to the main list area
	$("#list").empty();
	var isOpen;
	var placeButtonId; // element ID of a particular place's button
	var distance;
	displayedPlaces = [];
	$.each(sortedPlaces, function(i,item) {
		var showMe = filter(item);
		if (showMe) {
			// prep variables
			placeButtonId = "placeid_" + item.place_id;
			isOpen = calcIsOpen(item.place_id, time, daysFromNow);
			distance = calcDistance(item.position.latitude, item.position.longitude, userLocation.latit, userLocation.longit);
			
			// add location to list of displayed places
			$("#list").append("<li data-icon='info' data-theme='" + (isOpen ? "c" : "c") +"'><a href='#' id='"+ placeButtonId +"' data-rel='popup' data-position-to='window' data-transition='pop'>" +
				item.name + " " + distance + ((distance == "") ? "" : "ft") + "<span class='ui-li-count' " + (isOpen ? "style='color: #FFFFFF; background: #468847;" : "style='color: #FFFFFF; background: #b94a48;") +
				"'>" + (isOpen ? "Open" : "Closed") + "</span>" + "</a></li>");
			
			// Build a popup to be shown on clicking the location listing
			$('#'+placeButtonId).on('click', function() {
				//create a div for the popup
				var $popUp = $("<div/>").popup({dismissible: true, theme: "b", overlayTheme: "e", transition: "pop"})
				.on("popupafterclose", function () {$(this).remove();}) //remove when the popup is closed, delete it from visibilty
				.css({'padding': '5px'}); // set popup CSS styling
				
				// append popup content to popup div
				$popUp.append('<a id="closePopup" href="#" data-role="button" data-theme="a" data-icon="delete" data-iconpos="notext" class="ui-btn-left">Close</a>'); // popup close button
				$('#closePopup').click(function() {$popUp.popup("close");});
				
				$popUp.append(buildStandardPlaceInfoString(item));
				
				$('#place_location').replaceWith('<p id="place_location">' + "Location: <a id='popupMap' href=''>" + item.location + ' (Toggle Map)</a></p>'); // location with link
				$('#popupMap').click(function() {
					$('#' + placeButtonId + '-map').toggle();
					initializeMap(item.name,placeButtonId + '-map', item.position.latitude, item.position.longitude);
				});
				
				$('#place_location').after('<div id="' + placeButtonId + '-map" style="width: 100%; height: 30vh; display: none;"></div>');
				
				$popUp.popup('open').trigger("create"); // format popup using jqueryMobile
			});
			displayedPlaces.push(item);
		}
	});
	if ($('#list li').length == 0) $("#list").append("<li data-theme='d'><a href='#' id='empty'>No Results - Change Filter Options</a></li>"); // note empty display list
		
	if ( $('#list').hasClass('ui-listview')) { // update listview using jquery mobile
		$('#list').listview('refresh');
	} else {
		$('#list').trigger('create');
	}
	
	//Updates to the panel
	$("#showClosed>.ui-btn-inner>.ui-btn-text").text((showClosed ? "Hide" : "Show") + " Closed Venues"); //hide/show closed venues
	buildTagList(); // build list of tags from displayed places
	refreshTagList(); // update list of tags available to user in GUI
}

// builds basic HTML content for popups and map listings
function buildStandardPlaceInfoString(item) {
	var contentString = "<h1 id='place_title'>" + item.name + "</h1>";;
	
	contentString = contentString + '<p id="place_location">' + 'Location: ' + item.location + '</p>'; // location string
	
	if (calcDistance(item.position.latitude, item.position.longitude, userLocation.latit, userLocation.longit) != "") { //distance to location
		contentString = contentString + "<p id='place_distance'>Distance (ft): " + calcDistance(item.position.latitude, item.position.longitude, userLocation.latit, userLocation.longit) + "</p>";
	}
	
	if ("tags" in item) { // tag listing
		var tagString = "";
		$.each(item.tags, function(j,tag) { 
			tagString += ' "' + tag + '" ';
		});
		tagString = tagString.replace(/_/g," ");
		contentString = contentString + "<p>Tags: " + tagString + "</p>";
	}
	
	if ("schedule" in item) {
		contentString = contentString + "<p>Schedule Information: </p>"; // list schedule info
		var counter = 0;
		$.each(item.schedule, function(i, timeSlot) {
			counter++;
			if (counter < 10) contentString = contentString + "<p>&nbsp;&nbsp;&nbsp;&nbsp;" + timeSlot[0].date + ": " + timeSlot[0].time + " - " + timeSlot[1].time + "</p>";
		});
	}
	
	if ("extensions" in item) {
		$.each(item.extensions, function(j,extension) { // list extension data
			if (extension.value.indexOf("http") != -1) {
				contentString = contentString + '<p><a href="' + extension.value + '">' + extension.label + ' (External Link)</a></p>';
			} else {
				contentString = contentString + '<p>' + extension.label + ": " + extension.value + "</p>";
			}
		});
	}
	return contentString;
}

function createMapViewPopup() {
	//create a div for the popup
	var $popUp = $("<div/>").popup({dismissible: true, theme: "b", overlayTheme: "e", transition: "pop"})
	.on("popupafterclose", function () {$(this).remove();}) //remove when the popup is closed, delete it from visibilty
	.css({'padding': '5px'}); // set popup CSS styling
	
	// append popup content to popup div
	$popUp.append('<a id="closePopup" href="#" data-role="button" data-theme="a" data-icon="delete" data-iconpos="notext" class="ui-btn-left">Close</a>'); // popup close button
	$('#closePopup').click(function() {$popUp.popup("close");});
	
	$("<h1/>", {text: "Map View"}).appendTo($popUp); // title
	$popUp.append('<div id="mapView" style="width: 100%; height: 60vh; display: block;"></div>');
	$popUp.append('<p>In the above map you, you may click or tap on any marker of your choice to view more specific details regarding that partiular location.</p>');
	$popUp.popup('open').trigger("create"); // format popup using jqueryMobile
	
	var myLatLng = new google.maps.LatLng(displayedPlaces[0].position.latitude, displayedPlaces[0].position.longitude);
	
	var mapOptions = {
		zoom: 17,
		center: myLatLng,
		disableDefaultUI: false,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	}
	var map = new google.maps.Map(document.getElementById('mapView'), mapOptions);
	
	var latLngs = []
	$.each(displayedPlaces, function(i, item){
		var myLatLng = new google.maps.LatLng(item.position.latitude, item.position.longitude);
		latLngs[i] = myLatLng;
		var marker = new google.maps.Marker({
			position: myLatLng,
			map: map,
			title: item.name,
			zIndex: i
		});
		var contentString = buildStandardPlaceInfoString(item);
		
		var infowindow = new google.maps.InfoWindow({
			content: contentString
		});
		google.maps.event.addListener(marker, 'click', function() {
			infowindow.open(map,marker)
		});
	});
	
}			

// return boolean with if location is open
function calcIsOpen(placeId, passed_time, passed_date) {
	var found = false;
	var requestedPlace;
	var isOpen = false;
	$.each(localPlaces, function (j, place) { // search through cached places for a matching place
		if (!found && placeId == place.place_id) {
			found = true;
			requestedPlace = place;
		}
	});
	if (!found || !('schedule' in requestedPlace) || requestedPlace.schedule.length == 0) {
		return requestedPlace.open_now; // if no detailed schedule info available, default to open_now value
	}
	var start;
	var end;
	var tempNow = new Date();
	if (passed_time.length < 1) {
		passed_time = tempNow.toTimeString();
	}
	if (passed_date.length < 1) {
		passed_date = "0";
	}
	
	var evalTime = new Date(tempNow.toDateString() + " " + passed_time);
	evalTime.setDate(tempNow.getDate() + parseInt(passed_date));
	$.each(requestedPlace.schedule, function (j, timeSlot) {
		start = new Date(timeSlot[0].date + " " + timeSlot[0].time);
		end = new Date(timeSlot[1].date + " " + timeSlot[1].time);
		if (evalTime >= start && evalTime <= end) {
			isOpen = true;
		}
	});
	return isOpen;
}

//CompareTo for comparing two places by distance
function compareDist(a,b) {
	var distToB = calcDistance(b.position.latitude, b.position.longitude, userLocation.latit,userLocation.longit);
	var distToA = calcDistance(a.position.latitude, a.position.longitude, userLocation.latit,userLocation.longit);
	if (distToA == distToB) return ((b.name < a.name) ? 1 : -1);
	if (distToB == "" || isNaN(distToB)) return -1; // unknown distances at end of list
	if (distToA == "" || isNaN(distToA)) return 1;
	return distToA - distToB;
}

// distance calculation function for latitude/longitude
var calcDistance = function getDistanceFromLatLonInMi(lat1, lon1, lat2, lon2) {
    function deg2rad(deg) {
        return deg * (Math.PI / 180);
    }
    var R = 6371; // Radius of the earth in km
    var dLat = deg2rad(lat2 - lat1); // deg2rad below
    var dLon = deg2rad(lon2 - lon1);
    var a =
        Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
        Math.sin(dLon / 2) * Math.sin(dLon / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c; // Distance in km
    d = d * 0.621371; // Distance in mi
	d = d * 5280; // Distance in ft
	if (lat1+lon1 == 0 || lat2+lon2 == 0) d = Math.sqrt(-1);
    return isNaN(d) ? "" : d.toFixed(0);
};

//Builds List of possible tags from list of displayed Places
function buildTagList() {
	tagList = [];
	$.each(displayedPlaces, function(i,place) {
		if ("tags" in place) {
			$.each(place.tags, function(j,tag) {
				if (tagList.indexOf(tag) == -1 && tag.indexOf("campus") == -1 && filterTags.indexOf(tag) == -1) {
					tagList.push(tag);
				}
			});
		}
	});
}

// updates GUI listing of tags available to sort by
function refreshTagList() {
	$('#taglist').empty();
	$('#taglist').append('<option value="" selected="selected">Choose a Tag</option>');
	if (tagList.length < 1) {
		$('#taglist').append('<option value="" selected="selected">No Tags Remaining</option>');
	}
	$.each(tagList,function(i,tag) {
		$('#taglist').append('<option value="' + tag + '">' + tag.replace(/_/g," ") + '</option>');
	});
	$("#taglist").parent().trigger('create');
}

// initialize content (mostly just binding events to UI items)
function initContent() {
	$("#showClosed").click(function() {
		showClosed = !showClosed; // toggle showClosed
		refreshContent();
	});
	$("#campus").change(function() {
		refreshContent();
	});
	$("#time").change(function() {
		refreshContent();
	});
	$("#date").change(function() {
		refreshContent();
	});
	$("#taglist").change(function() {
		var tempString = $("#taglist").val();
		if ( tempString != "") {
			el = $("<a href='#' id='" + tempString + "' data-role='button' data-theme='b' data-icon='delete' data-iconpos='left'>" + tempString.replace(/_/g," ") + "</a>").on("click", function() {
				$(this).remove();
				for (var i = 0; i < filterTags.length; i++) {
					if (filterTags[i] === tempString) { 
					filterTags.splice(i, 1);
					break;
					}
				}
				refreshContent();
			});
			filterTags.push(tempString);
			$("#selected-tags").append(el);
			$("#selected-tags").trigger('create');
			refreshContent();
		}
	});
}

var localPlaces = []; // just an empty array in case foodapp.js is not fully loaded yet. (see foodapp.js for full description)
var displayedPlaces = []; // list of places currently being displayed
var tagList = []; // list of all tags contained in displayedPlaces array
var campus = ""; // used to filter by campus location (equal to campus to be viewed)
var time = ""; // used to filter by time other than right now (equal to time to be viewed)
var daysFromToday = ""; // used to filter by day other than today (equal to days from now to be viewed)
var filterTags = []; // used to filter by a subset of all tags (equal to tags to be displayed)
var showClosed = false; // used to display closed 


initContent(); // bind filter events to filter GUI components
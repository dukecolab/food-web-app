DukeFood README File

Abstract:
	This application is designed to allow students and faculty to view and list all eateries and food-sources available on duke campus.

The User Experience:
	1) This application has a (comparatively) slow load-time for the first time a user visits the page, but all loaded data is cached immediately
		This load time is further reduced due to an intentionally limted use of external frameworks and supporting files.
		In order to speed the process of access to relevant data, all requests are completed asynchronously, and data is immediately loaded into the display on its availability
			This means that as more complex and detailed information is downloaded, in an iterative process, the data is given to the user immediately, rather than waiting for all the data to be loaded fully
	2) The second time the page is loaded, the entire page has already been saved in the browser HTML5 Application Cache (This is implemented in the 'offline' branch on gitorious, as two unsolved UI bugs occur)
		Incidentally, this also makes the entire application available OFFLINE and WITHOUT internet access.
	3) In order to accomodate this offline functionality, all data is stored in the local storage of the browser after it has been downloaded
	4) The end result is that after a person vists the page once, they can utilize all the data they downloaded for at least a week before it becomes outdated and they require an internet connection to refresh it
		We opted to not use data extrapolation (i.e. adding 1 week to old data to guess what the schedule would be the following week) in order to preserve data accuracy for the user
		Additionally, all subsequent page-loads are completed nearly instantly for the user using cached data which is then refreshed and updated if an internet connection is available
	5) All filtering, sorting and display of content is processed in the users browser, meaning there is next to no load on the server hosting the service.
		The only requirements of the server is that it be capable of serving text/manifest filetypes and control the expire data for browser caching (to prevent caching the HTML5 cache manifest)

Design:

DukeFood is compartmentalized into 2 distinct portions:
	A) the backend, which requests data from online sources using jquery/ajax requests and parsing/combining the JSON responses into a local, uniform dataset that can be cached in the local browser and fetched at will.
	B) the frontend, which builds all the dynamic portions of user content, including applying user-chosen filtering rules and tag information

Data is transferred between these two components using the localPlaces array.

	Design goals: 
		Minimize time from opening application to seeing relevant data
		Maximize functionality without an internet connection
		Maximize modularity on the server-side/Minimize dependencies
		Maximize compatibility across all devices
		Maintain a native "feel" of the application
		All of the above, WITHOUT sacrificing functionality, user experience or extensibility to new data sources
		
		We chose to focus on load time and caching because typically students want access to this information instantly, or faster than anyone else in the room can be bothered to remember what's open when.


	Backend Summary:
		NOTE: We designed this system to be extremely extensible to new data sources. If anyone at Duke/HDRL/RLHS etc. is willing to commit to maintaining a google calendar of the merchants-on-points delivery data, it takes only 1 additional line of code to add support for this data source.

		The steps of a pageload from the backend perspective are as follow:
			1) Fetch most-recently cached data immediately on page-load (and update GUI)
			2) Get user's GPS location and use it to calculate distance to locations (and update GUI)
			3) Query Duke's Streamer Service to obtain the most recent data about places (And update GUI)
				3.5) Query Streamer Service for every dining location to acquire detailed scheduling information (and update GUI)
					Note: This step can be modified to only occur under certain circumstances if this causes too many API calls or too much of a server load
			4) Query Google calendar containing food truck schedule and information (and update GUI)

	Frontend Summary:
		The entire frontend system hinges on the refreshContent() function, which is called anytime an event occurs which could have potentially altered the data the user is attempting to view.
			This includes the filtering and sorting of content
		
		The frontend is mostly just responsive to user interaction. In other words, all of the functions in foodgui.js are called only when the user clicks on the correct UI item.

Potential Feature List:
	User filter option for sorting for days in the future (originally planned but not yet implemented)
	User settings for lower data usage (only download after data expires for example)
